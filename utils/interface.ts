export interface IClient {
    _id: string,
    username: string,
    email: string,
    password: string,
    ville: string,
    contact : number,
    cretedAt: Date,
    updatedAt: Date,
}
export interface IMenu {
    _id: string,
    categorie: string,
    prix: number,
    description: string,
    idResto: string,
    cretedAt: Date,
    updatedAt: Date,
    libelle: string
}
export interface IRestaurant {
    _id: string,
    nomResto: string,
    emailResto: string,
    localisation: string,
    contactResto: number,
    heureOpen: string,
    heureClose: string,
    statutResto: string,
    idProprio: string,
    commandeGlobale: [],
    clientGlobale: [],
    cretedAt: Date,
    updatedAt: Date
}
export interface ICategorie {
    _id: string,
    libelle: string,
    description: string,
    cretedAt: Date,
    updatedAt: Date
}
export interface ITable {
    _id: string,
    numeroTable: number,
    commandeAssocie: string,
    clientAssocie: string,
    dateHeureOccupation: Date,
    statutTable: string,
    idResto: string,
    cretedAt: Date,
    updatedAt: Date
}
export interface ICommande {
    _id: string,
    numeroCommande: number,
    idClient: string,
    idResto: string,
    idTable: string,
    idMenu: string,
    quantite: number,
    montantTotal: number,
    dateHeureCommande: Date,
    statutCommande: string,
    cretedAt: Date,
    updatedAt: Date
}
export interface IProprio{
    _id: string,
    nomProprio: string,
    emailProprio: string,
    contactProprio: number,
    idResto: string,
    ville: string,
    Pays: string,
    idRestoAssocie: string,
    cretedAt: Date,
    updatedAt: Date

}
export interface AuthRequest extends Request {
    [x: string]: any;
    user?: IClient
}