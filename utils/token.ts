import { sign, verify } from "jsonwebtoken";
export const generateToken = (user: string): string => {
  const SECRET_KEY = process.env.SECRET_KEY;
  if (!SECRET_KEY) throw new Error("SECRET_KEY is not defined");
  return sign(user, SECRET_KEY, { expiresIn: "24h" });
};

export const verifyToken = (token: string): boolean| string| object => {
  try {
    const SECRET_KEY = process.env.SECRET_KEY;
    if (!SECRET_KEY) throw new Error("SECRET_KEY is not defined");
    return verify(token, SECRET_KEY);
  } catch (error) {
    console.log(error);
    return false;
  }
};
