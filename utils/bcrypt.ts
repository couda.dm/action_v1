import {hash, compare, genSalt} from 'bcrypt';
export const hashPassword = async (mdp: string) => {
    const salt = await genSalt(10);
    return await hash(mdp, salt);
};

export const decoded = async (newMdp:string, oldMdp:string) =>{
    try {
        return await compare(newMdp, oldMdp);
    } catch (e) {
        return false;
    }
}