import { Request, Response, NextFunction } from "express";
import { verifyToken } from "../utils/token";
import { AuthRequest, IClient } from "../utils/interface";

export const auth = (
  req: AuthRequest,
  res: Response,
  next: NextFunction
): Response | void => {
    const token: string | undefined =
        req.cookies.token || (req.headers as { authorization?: string })?.authorization?.split(" ")[1];
    const isTokenExist =
        typeof token === "string"
            ? verifyToken(token)
            : res.status(401).json({ message: "Unauthorized" });

    if (typeof isTokenExist === "object") {
        req.user = isTokenExist as IClient;
        next();
    } else {
        res.redirect("/login");
    }
};
