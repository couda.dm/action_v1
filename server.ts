import cookieParser from "cookie-parser";
import express from "express";
import cors from "cors";
import path from "path";
import { config } from "dotenv";
import { connectDB } from "./config/db";
import { serve, setup } from "swagger-ui-express";
// import swaggerFile from "./utils/swagger.json";
config({
  path: path.join(process.cwd(), ".env.local"),
});

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("assets"));
app.use(cookieParser());
app.use(cors());

connectDB()
  .then(() => {
    app.listen(3000, () => {
      console.log(`Server is running on PORT ${process.env.PORT}`);
    });
  })
  .catch((e) => {
    console.log(e.message);
  });

// app.use("/", serve, setup(swaggerFile));