import {connect} from 'mongoose';
import { inProduction } from './env';

export const connectDB = async () => {
    try {
        const MONGO_URI = process.env.MONGO_URI;
        if(!MONGO_URI) throw new Error('MONGO_URI is missing');
        await connect(MONGO_URI, {
            dbName: inProduction ? process.env.DB_NAME :'fastAction-test'
        });
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
}