export const inProduction = process.env.NODE_ENV === 'production';
export const inDevelopment = !inProduction;
export const inTest = process.env.NODE_ENV === 'test';
export const port = process.env.PORT;
export const APIurl =  inProduction?'':`http://localhost:${process.env.PORT}`;